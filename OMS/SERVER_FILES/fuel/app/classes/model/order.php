<?php
/*
* The Order Management System Model Class	
* 
* @access   public
* @extends  Model
*/
namespace Model;
use DB;
class Order extends \Model {
	/*
	* Store order in the database	
	* @access  public
	* @param   String   A JSON object conforming to the Order Object specification 
	* @return  int      0 on failure, 1 on success
	*/
	public static function setOrder($json_order_string)
	{
		$json_array = json_decode(trim(html_entity_decode($json_order_string)), true);
		
		$order = array('USER_NAME', 'ORDER_NUMBER', 'TABLE_NUMBER', 'MENU_ITEM', 'OPTIONS');
		
		// Validate JSON object
		if(!isset($json_array['userID']) || $json_array['userID'] == '')
			return 0;
		$userID = $json_array['userID'];
		$user_record = Order::getUser($userID);
		if($user_record == -1)
			return -1;
		$userName = $user_record[0]['USER_NAME'];
		if(!isset($json_array['orderNumber']) || $json_array['orderNumber'] == '')
			return 0;
		$orderNumber = $json_array['orderNumber'];
		if(!isset($json_array['tableNumber']) || $json_array['tableNumber'] == '')
			return 0;
		$tableNumber = $json_array['tableNumber'];
		if(!isset($json_array['orderContents']) || $json_array['orderContents'] == '')
			return 0;
		$orderContents = $json_array['orderContents'];
		$optionstr = '';
		$menuItem = '';
		foreach($orderContents as $orderParts)
		{
			if(is_array($orderParts))
			{   
				// Order must contain at least one menu item
				if(!isset($orderParts['menuItem']))
					return 0;
				if($orderParts['menuItem'] == '')
					return 0;
				foreach($orderParts as $order_items)
				{				
					if(is_array($order_items))
					{
						$iter = new \CachingIterator(new \ArrayIterator($order_items));
						foreach($iter as $key=>$item_options)
						{	
							// Pull out options and comma seperate
							foreach($item_options as $option)
								$optionstr = $optionstr . ' ' . $option . ',';
							// When last option is reached, make and execute SQL command
							if(!$iter->hasNext())
							{
								//All the info has been gathered from the array for one main menu item. 
								//Make the query and execute.
								$options = trim(rtrim($optionstr, ','));
								$val = array($userName, $orderNumber, $tableNumber, $menuItem, $options);					
								$query = DB::insert('orders');
								$query->columns($order);
								$query->values($val)->execute();
								// Reset certain strings
								$optionstr = '';
								$menuItem = '';
							}
						}	
					}
					else
					{
						$menuItem = $order_items;
					}	
				}	
			}
		}
		// successful database insert
		$success = 1;
		return  $success;
	}
	
	/*
	* Retrieve a single order in the database	
	* @access  public
	* @param   String   The number of the order to retrieve 
	* @return  String   A JSON string in Order Object format
	*/
	public static function getOrder($orderNumber)
	{
		$query = DB::query("SELECT * FROM orders WHERE id = $orderNumber");
		$result = $query->execute()->as_array();
		//TODO: Convert to JSON  $json = Format::forge($array)->to_json() as time allows
		return $result;	
	}
	
	/*
	* Retrieve all unfulfilled orders	
	* @access  public
	* @return  String  A JSON string in Order Object format
	*/
	public static function getActiveOrders()
	{
		$query = DB::query("SELECT USER_NAME, TABLE_NUMBER, MENU_ITEM, OPTIONS FROM orders WHERE DELIVERED = 0");
		$result = $query->execute()->as_array();
		$as_json = json_encode($result);
		return $as_json;
	}
	
	/*
	* Retrieve user information	
	* @access  public
	* @param   String  The id of the user 
	* @return  array   An associative array containing user data
	*/
	public static function getUser($userID)
	{
		$query = DB::select()->from('users')->where('ID', intval($userID));
		$result = $query->execute()->as_array();
		//print_r($result);
		if(empty($result))
			return -1;
		return $result;
	}
	
	/*
	* Retrieve all orders	
	* @access  public
	* @return  String  A JSON string in Order Object format
	*/
	public static function getAllOrders()
	{
		//TODO: Implementation TBD
	}
	
	/*
	* Flag an order as having been fulfilled	
	* @access  public
	* @param   String  The number of the order
	* @return  String  A JSON string in Order Object format
	*/
	public static function setOrderComplete($orderNumber)
	{
		$query = DB::update('orders')->set(array('DElIVERED' => 1))->where('ORDER_NUMBER', $orderNumber);
		$results = $query->execute();
		//A successful update returns $results=0, this is inconsistent with error return codes in the other
		//methods of this model, so the return values are flipped.
		if($results == 0)
			return 1;
		return 0;
	}
	
    /*
	* Add user to database
	* @param   String  The user name
	* @return  String  The id of the new user
	*/
	public static function setUser($userName)
	{
		$results = DB::select()->from('users')->where('USER_NAME', $userName)->execute()->as_array();
		$user_name_status_message = "The user $userName already exists.";
		if(!empty($results))
			return $user_name_status_message;
		$query = DB::insert('users');
		$column = array('USER_NAME');
		$val = array($userName);
		$query->columns($column);
		$query->values($val)->execute();
		$results = DB::select()->from('users')->where('USER_NAME', $userName)->execute()->as_array();
		return $results;
	}
}

