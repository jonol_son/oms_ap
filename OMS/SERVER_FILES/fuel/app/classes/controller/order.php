<?php
/**
 * The Order Controller.
 * @package  app
 * @extends  Controller
 */
use \Model\Order;
class Controller_Order extends Controller
{
	// Default controller method
	public function action_index()
	{
		$data['jsonIn'] = "INDEX IS DISABLED.";
		return Response::forge(View::forge('orderer/orderview', $data));
	}
	
	/*
	* Submit an order 
	* @access  public
	* @return  Response
	*/
	public function action_submitOrder()
	{
		$json_order_string = $this->param('order');
		
		// Validate json, return an invalid json message to client if json cannot be decoded
		if(json_decode(html_entity_decode($json_order_string)) == null)
		{
			$body = 'Invalid JSON';
			$response = new Response($body, 400);
			return $response;
		}
		$results = Order::setOrder($json_order_string);
		
		//Order successfully inserted in database
		if($results == 1)
		{
			$body = '</br></br>Order Successfully Submitted';
			$response = new Response($body, 200);
			return $response;
		}
		if($results == -1) // User ID not valid 
		{
			$body = '</br></br>The User ID is not valid.';
			$response = new Response($body, 400);
			return $response;
		}
		else //Order insertion unsuccessful
		{
			$body = '</br></br>Order Submission Failed';
			$response = new Response($body, 500);
			return $response;
		}
		return Response::forge(View::forge('orderer/orderview', $data));
	}
	
	/*
	* Retrieve an order. 
	* @access  public
	* @return  Response
	*/
	public function action_getOrder()
	{
		$orderNumber = $this->param('oNumber');
		$results = Order::getOrder($orderNumber);
		$data['order'] = $results;
		return Response::forge(View::forge('orderer/orderview', $data));
	}
	
	/*
	* Retrieve all orders. 
	* @access  public
	* @return  Response
	*/
	public function action_getOrders()
	{
		$results = Order::getActiveOrders();
		$body = $results;
		$response = new Response($body, 200);
		return $response;
	}
	
	/*
	* Add a user.
	* @access  public
	* @return  Response
	*/
	public function action_addUser()
	{
		$userName = $this->param('name');
		$results = Order::setUser($userName);
		$data['userID'] = $results;
		return Response::forge(View::forge('orderer/orderview', $data));
	}
	
	/*
	* Retrieve the user name of a given user id.
	* @access  public
	* @return  Response
	*/
	public function action_getUser()
	{
		$userID = $this->param('id');
		$results = Order::getUser($userID);
		$data['userName'] = $results[0]['USER_NAME'];
		return Response::forge(View::forge('orderer/orderview', $data));
	}
	
	/*
	* Flag an order as having been fulfilled.
	* @access  public
	* @return  Response
	*/
	public function action_setComplete()
	{
		$orderNumber = $this->param('o_num');
		$results = Order::setOrderComplete($orderNumber);
		$bad_order_message = 'Order number does not exist';
		if($results == 0)
			$data['orderComplete'] = $bad_order_message;
		else
			$data['orderComplete'] = 'Order Complete';
		return Response::forge(View::forge('orderer/orderview', $data));
	}
}