<?php
return array(
     //URI          CONTROLLER METHOD
	'_root_'  => 'welcome/index',  // The default route
	'_404_'   => 'welcome/404',    // The main 404 route
	//'order(/:json)?'   => array('order/postOrder', 'json' => 'error'),
    //'submitOrder(/:json)?' => 'order/submitOrder',
	
	// These parameter names, such as "/:order" MUST be ALL lowercase or Fuel can't find the controller method.
	'submitOrder/:order'   => 'order/submitOrder',
	'getOrders' => 'order/getOrders',
	'addUser/:name' => 'order/addUser',
	'getUser/:id' => 'order/getUser',
	'setComplete/:o_num' => 'order/setComplete',
	'order(/:json)?' => 'order/postOrder', 
	'hello(/:name)?' => array('welcome/hello', 'name' => 'hello'),
);