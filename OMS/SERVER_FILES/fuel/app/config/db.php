<?php
/**
 * Base Database Config.
 *
 * See the individual environment DB configs for specific config information.
 */

return array(
	'active' => 'development',

	/**
	 * Base config, just need to set the DSN, username and password in env. config.
	 */
	'default' => array(
		'type'        => 'pdo',
		'connection'  => array(
			'persistent' => false,
		),
		'identifier'   => '`',
		'table_prefix' => '',
		'charset'      => 'utf8',
		'enable_cache' => true,
		'profiling'    => false,
	),

	'redis' => array(
		'default' => array(
			'hostname'  => '127.0.0.1',
			'port'      => 6379,
		)
	),
// a MySQL driver configuration
'development' => array(
    'type'           => 'mysql',
    'connection'     => array(
        'hostname'       => 'localhost',
        'port'           => '3306',
		'database'       => 'oms',
        //'database'       => 'chris_wordpress',
        'username'       => 'root',
        'password'       => '',
        'persistent'     => false,
    ),
    'table_prefix'   => '',
    'charset'        => 'utf8',
    'enable_cache'   => true,
    'profiling'      => false,
),
'experimental' => array(
    'type'           => 'mysql',
    'connection'     => array(
        'hostname'       => 'localhost',
        'port'           => '3306',
        'database'       => 'ordermanagement',
        'username'       => 'root',
        'password'       => '',
        'persistent'     => false,
    ),
    'table_prefix'   => '',
    'charset'        => 'utf8',
    'enable_cache'   => true,
    'profiling'      => false,
),

);
