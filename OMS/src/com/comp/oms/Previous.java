package com.comp.oms;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ListActivity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Previous extends ListActivity implements OnClickListener{
	String viewStrings[];
	String response = "initial";
	private static final String DEBUG_TAG = "display_orders";
	
	/* TODO 
	 * Display Orders and Redisplay Orders
	 * Request order completion
	 * */

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(isLive()) {
			// Live
			AsyncTask task = new DownloadWebpageTask().execute("http://24.4.235.42:8080/getOrders");
		
			// 10.0.2.2 is a special alias to the loopback interface on the development machine
			//AsyncTask task = new DownloadWebpageTask().execute("http://10.0.2.2/glam_fuel/public/getOrders");
			//AsyncTask task = new DownloadWebpageTask().execute("http://10.0.0.19:8080/getOrders");
		}
		else {
			// TODO : Display "Network Connection Unavailable"
			viewStrings = new String[]{"Network Unavailable"};
		}  
   	}
	private String[] responseToViewStringArray(String response) 
	{	
		String result[];
		try{
			JSONArray jsonArray = new JSONArray(response);
			result = new String[jsonArray.length()];
			if(jsonArray.length() == 0) return null;
			for(int i = 0; i < jsonArray.length(); i++)
			{
				JSONObject jObj = jsonArray.getJSONObject(i);
				result[i] = jObj.get("TABLE_NUMBER") + ":" + jObj.get("USER_NAME") + " " + jObj.get("MENU_ITEM") + " +" + jObj.get("OPTIONS");
			}
		}
		catch (Exception e){
			result = new String[]{"Invalid JSON Syntax"};
		}
		return result;
	}
	// Uses AsyncTask to create a task away from the main UI thread. This task takes a 
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
    private class DownloadWebpageTask extends AsyncTask<String, Void, String> 
    {  
       @Override
       protected String doInBackground(String... urls) 
       {            
           // params comes from the execute() call: params[0] is the url.
           try {
               return downloadUrl(urls[0]);
           } catch (IOException e) {
               return "Unable to retrieve web page. URL may be invalid.";
           }
       }
       // onPostExecute displays the results of the AsyncTask.
       @Override
       protected void onPostExecute(String result) 
       {
    	   viewStrings = responseToViewStringArray(result);
    	   setListAdapter(new ArrayAdapter<String>(Previous.this,
   				android.R.layout.simple_list_item_1, viewStrings)); ;
       }
    }
    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl) throws IOException 
    {
    	InputStream is = null;
	    // Only display the first 2000000 characters of the retrieved
	    // web page content.
	    int len = 2000000;
         
        try {
        	URL url = new URL(myurl);
		    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		    conn.setReadTimeout(10000 /* milliseconds */);
		    conn.setConnectTimeout(15000 /* milliseconds */);
		    conn.setRequestMethod("GET");
		    conn.setDoInput(true);
		    // Starts the query
		    conn.connect();
		    int response = conn.getResponseCode();
    	
	 	    // Check to see if our order submission failed 
		    if(response != 200)
			    System.out.println("Order Submission Failed");  
		    else
			    System.out.println("Order Submission Successful");

		    Log.d(DEBUG_TAG, "The response is: " + response);
		    is = conn.getInputStream();

		    // Convert the InputStream into a string
		    String contentAsString = readIt(is, len);
		    return contentAsString;
         
		    // Makes sure that the InputStream is closed after the app is
		    // finished using it.
	    } finally {
	    	if (is != null) {
	    		is.close();
	    	}
	    }      
    }   
    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");        
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
		
	private boolean isLive(){
		ConnectivityManager connMgr = (ConnectivityManager) 
		        getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		else {
			return false;
		}
		
	}

	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

}