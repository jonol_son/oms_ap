package com.comp.oms;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class Splash extends Activity {

	//initialize mediaplayer to access in all methods
	MediaPlayer ourSong;
	
	@Override
	protected void onCreate(Bundle OMS) {
		// TODO Auto-generated method stub
		super.onCreate(OMS);
		setContentView(R.layout.splash);
		
		//define under content playsong
		ourSong = MediaPlayer.create(Splash.this, R.raw.dinner_platesound);
//		ourSong = MediaPlayer.create(Splash.this, R.raw.ohshi);
		ourSong.start();
		
		//set up timer to wait 5 sec
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(2500);
				} catch (InterruptedException e){
					e.printStackTrace();
				}finally{
					//set up new intent using action name from manifest
					Intent openStartingPoint = new Intent("com.comp.oms.PASSWORD");
					startActivity(openStartingPoint);
				}
			}
		};
		timer.start();
	}
	protected void onPause() {
		super.onPause();
		ourSong.release();
		finish();
	}
		
}
