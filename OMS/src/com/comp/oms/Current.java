package com.comp.oms;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.comp.oms.Submit;

import android.widget.Toast;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;



public class Current extends Activity implements View.OnClickListener {

	Button add, remove, submit;
	// array for added items
	List<String> SpinnerArray = new ArrayList<String>();
	// array for table number
	List<Integer> SpinnerArray2 = new ArrayList<Integer>();
	Random rando = new Random();
	int orderNumber = rando.nextInt(100);
	String list[] = { "Add" };
	String table, order, newItem, itemToBeDeleted;
	Spinner addedItems;


	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		setContentView(R.layout.current);
		add = (Button) findViewById(R.id.addItem);
		remove = (Button) findViewById(R.id.removeItem);
		submit = (Button) findViewById(R.id.submitOrder);
		// initially draw order spinner
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, SpinnerArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		addedItems = (Spinner) findViewById(R.id.spinner1);
		addedItems.setAdapter(adapter);
		//Click listener for the added items
		addedItems.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> adapter2, View arg1,
					int arg2, long arg3) {
				itemToBeDeleted = adapter2.getItemAtPosition(arg2).toString();
			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
//debug

		
		add.setOnClickListener(this);
		submit.setOnClickListener(this);
		remove.setOnClickListener(this);
		// for tables
		fillTableSpinner();

		/*
		 * //TEST XXXXXXXXXXXXXXX Intent ourIntent = getIntent(); String next =
		 * (String)ourIntent.getSerializableExtra("order"); int count = 0; if
		 * (Order.isEmpty() == true){ list[0] = next; Order.add(0, next); } else
		 * { Order.add(Order.size(), next); while (Order.isEmpty()!=true){
		 * list[count] = Order.get(count); count++; } }
		 */
		// set up list adapter using a stock int as 2nd input
		// setListAdapter(new ArrayAdapter<String>(Current.this,
		// android.R.layout.simple_list_item_1, list));
	}

	private void fillTableSpinner() {
		// TODO Auto-generated method stub
		for (int i = 1; i < 20; i++) {
			SpinnerArray2.add(i);
		}
		ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(this,
				android.R.layout.simple_spinner_item, SpinnerArray2);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner tableNumber = (Spinner) findViewById(R.id.spinner2);
		tableNumber.setAdapter(adapter2);
		// CLICK LISTENER FOR Table Number
		tableNumber.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> adapter2, View arg1,
					int arg2, long arg3) {
				table = adapter2.getItemAtPosition(arg2).toString();

			}

			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addItem:

			// set up intent with class variable, need catch incase
			// classnotfound
			// will start a class variable with WHICHEVER string is selected
			// In mainfest you nolonger need intent filter if you use this

			Intent i = new Intent(this, Menu.class);
			startActivityForResult(i, 1);
			break;

		case R.id.removeItem:
			SpinnerArray.remove(itemToBeDeleted);
			// REDRAW SPINNER after removing
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, SpinnerArray);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			addedItems.setAdapter(adapter);
			break;
			
		case R.id.submitOrder:
			
			
			//Setup JSON object
			String json = "submitOrder/{&quot;userID&quot;:1,&quot;orderNumber&quot;:";
			
			//Attach an order number
			double randNumber = Math.random();
			double d = randNumber *300;
			int rand = (int)d;
			json = json + rand + ",&quot;tableNumber&quot;:";
			
			//set table number
			json = json + table + ",&quot;orderContents&quot;:[{&quot;menuItem&quot;:&quot;";
			
			
			for (int iter = 0; iter < SpinnerArray.size(); iter++){
				if (SpinnerArray.size() == 1)
					json = json + SpinnerArray.get(iter);
				else
				{
					json = json + SpinnerArray.get(iter) + "&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;&quot;},{&quot;optionItem2&quot;:&quot;&quot;}]},{&quot;menuItem&quot;:&quot;";
					//json = json + SpinnerArray.get(iter) + "&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;lettuce&quot;},{&quot;optionItem2&quot;:&quot;tomatoes&quot;}]},{&quot;menuItem&quot;:&quot;";
				}
			}
			
			json = json + "&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;&quot;},{&quot;optionItem2&quot;:&quot;&quot;}]}]}";
			//json = json + "&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;cheese&quot;},{&quot;optionItem2&quot;:&quot;carrots&quot;}]}]}";
			
			//debug.setText(json);
			
			
			//Stock sting
			//String json = "submitOrder/{&quot;userID&quot;:1,&quot;orderNumber&quot;:41,&quot;tableNumber&quot;:666,&quot;orderContents&quot;:[{&quot;menuItem&quot;:&quot;ApplePie&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;lettuce&quot;},{&quot;optionItem2&quot;:&quot;tomatoes&quot;}]},{&quot;menuItem&quot;:&quot;ChickenCurry&quot;,&quot;options&quot;:[{&quot;optionItem1&quot;:&quot;cheese&quot;},{&quot;optionItem2&quot;:&quot;carrots&quot;}]}]}";
			
			//link to chris stuff
			ConnectivityManager connMgr = (ConnectivityManager) 
			        getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
			if (networkInfo != null && networkInfo.isConnected()) {
				//EditText editText = (EditText) findViewById(R.id.edit_message);
				//String message = editText.getText().toString();
				//intent.putExtra(DEBUG_TAG, message);
				
				Intent intent = new Intent(this, Submit.class);
				intent.putExtra("json", json);

				startActivity(intent);
			} else {
			    // display error	    
		    }
			break;
			
		}
	}


	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1) {

			if (resultCode == RESULT_OK) {
				Toast.makeText(this, data.getData().toString(),
						Toast.LENGTH_SHORT).show();
				String choice = data.getStringExtra("choice");
				SpinnerArray.add(choice);

				// REDRAW SPINNER after returning
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, SpinnerArray);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				addedItems.setAdapter(adapter);
			}

			if (resultCode == RESULT_CANCELED) {
				// Write your code if there's no result
			}
		}

	}// onActivityResult

}
