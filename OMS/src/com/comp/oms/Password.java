package com.comp.oms;




import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

public class Password extends Activity implements OnClickListener{

	//moving variable out of method to access in all methods
	Button chkCmd;
	ToggleButton passTog;
	EditText uname;
	EditText pass;
	TextView display;
	int count = 5;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.alogin);
		
		cleanOnCreate();
		chkCmd.setOnClickListener(this);
		passTog.setOnClickListener(this);
		
	}

	private void cleanOnCreate() {
		// TODO Auto-generated method stub
		chkCmd = (Button) findViewById(R.id.bResults);
		//Declare it as final bc the reference should never change
		passTog = (ToggleButton) findViewById(R.id.tbPassword);
		uname = (EditText) findViewById(R.id.username);
		pass = (EditText) findViewById(R.id.password);
		display = (TextView) findViewById(R.id.tvResults);

	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		//v being passed int
		switch(view.getId()){
		case R.id.bResults:
			//get text from box and set it to a string
			String checkpass = pass.getText().toString();
			String checkuser = uname.getText().toString();
		
			if (!checkpass.contentEquals("p")){
//			if ((checkpass.contentEquals("oms") || checkpass.contentEquals("Oms") || checkpass.contentEquals("OMS"))&& ( checkuser.contentEquals("oms") || checkuser.contentEquals("Oms") || checkuser.contentEquals("OMS"))) {
				try {
					Class ourClass = Class.forName("com.comp.oms.Order");
					Intent ourIntent = new Intent(Password.this, ourClass);
					startActivity(ourIntent);
				}catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
			else {
				if(count >=2){
					display.setText("invalid password you have " + count + " remaining tries");
					display.setGravity(Gravity.CENTER);
					display.setTextColor(Color.BLACK);
				}
				if (count == 1){
					display.setText("invalid password you have " + count + " remaining try");
					display.setTextColor(Color.RED);
				}
				if (count == 0)
					finish();
				--count;
			}
			break;
			
		case R.id.tbPassword:
			if (passTog.isChecked()) {
				pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			}
			else{
				pass.setInputType(InputType.TYPE_CLASS_TEXT);
			}
			break;
		}
	}

	protected void onPause() {
		super.onPause();
		finish();
	}
}