package com.comp.oms;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class Order extends Activity implements OnClickListener{

	//moving variable out of method to access in all methods
	Button newO;
	Button existO;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order);
		
		cleanOnCreate();
		newO.setOnClickListener(this);
		existO.setOnClickListener(this);
		
	}

	private void cleanOnCreate() {
		// TODO Auto-generated method stub
		existO = (Button) findViewById(R.id.newOrder);
		newO = (Button) findViewById(R.id.prevOrder);
		
	}

	@Override
	public void onClick(View view) {
		// TODO Auto-generated method stub
		//v being passed int
		switch(view.getId()){
		case R.id.prevOrder:

			try {
				Class ourClass = Class.forName("com.comp.oms.Previous");
				Intent ourIntent = new Intent(Order.this, ourClass);
				startActivity(ourIntent);
			}catch (ClassNotFoundException e) {
				e.printStackTrace();	}			
			break;
			
		case R.id.newOrder:
			try {
				Class ourClass = Class.forName("com.comp.oms.Current");
				Intent ourIntent = new Intent(Order.this, ourClass);
				startActivity(ourIntent);
			}catch (ClassNotFoundException e) {
				e.printStackTrace();}
			break;
		}
	}

//	protected void onPause() {
//		super.onPause();
//		finish();
//	}
}