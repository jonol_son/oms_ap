package com.comp.oms;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;


public class Submit extends Activity {
	public static String APP_TAG = "";
	private static final String DEBUG_TAG = "HttpExample";
	TextView textView;
	
	String jsonGlobal;
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.submitting);
		//WebView webview = new WebView(this);
		//textView = new TextView(this);
		//setContentView(textView);
		
		// Make sure we're running Honeycomb or higher to use ActionBar APIs
		/*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            // Show the Up button in the action bar.
            getActionBar().setDisplayHomeAsUpEnabled(true);
        
        }*/
		// CHRIS'S GARBAGE==================================
		// Get the message from the intent
		//Intent intent = getIntent();
		//String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		
		// Create the text view
		//textView = new TextView(this);
		//textView.setTextSize(14);
		//textView.setText(message);
		
        // fetch data
		//WebView myWebView = (WebView) findViewById(R.id.webview);
		//webview.loadUrl("http://10.0.0.19:8080/ordertest.php");
		//String json = "{\"userID\":1,\"orderNumber\":69,\"tableNumber\":666,\"orderContents\":[{\"menuItem\":\"Steak\",\"options\":[{\"optionItem1\":\"onions\"},{\"optionItem2\":\"potatoes\"}]},{\"menuItem\":\"Fish\",\"options\":[{\"optionItem1\":\"cheese\"},{\"optionItem2\":\"carrots\"}]}]}";
		//====================================================================================
		
		
		// TODO Create a function to make this JSON given a list, array, or comma separated string
		// GOTTA USE HTML ESCAPED STRING
		if (savedInstanceState == null){
			String extras = getIntent().getExtras().getString("json");
			if (extras == null){
				jsonGlobal = null;
			} else {
				jsonGlobal = extras;
			}
		}
		
		//	new DownloadWebpageTask().execute("http://10.0.0.19:8080/ordertest.php");
		
		//                                 http://24.4.235.42:8080
		//new DownloadWebpageTask().execute("http://10.0.0.19:8080/" + json);
		new DownloadWebpageTask().execute("http://24.4.235.42:8080/" + jsonGlobal);
		//finish();

	}
	// Uses AsyncTask to create a task away from the main UI thread. This task takes a 
    // URL string and uses it to create an HttpUrlConnection. Once the connection
    // has been established, the AsyncTask downloads the contents of the webpage as
    // an InputStream. Finally, the InputStream is converted into a string, which is
    // displayed in the UI by the AsyncTask's onPostExecute method.
 private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
       @Override
       protected String doInBackground(String... urls) {
             
           // params comes from the execute() call: params[0] is the url.
           try {
               return downloadUrl(urls[0]);
           } catch (IOException e) {
               return "Unable to retrieve web page. URL may be invalid.";
           }
       }
       // onPostExecute displays the results of the AsyncTask.
       @Override
       protected void onPostExecute(String result) {
 //   	   textView.setTextSize(8);
 //        textView.setText(result);
    	   Toast.makeText(getApplicationContext(), "Order Submitted!",
					Toast.LENGTH_LONG).show();
			try {
				Class ourClass;
				ourClass = Class.forName("com.comp.oms.Order");
				Intent ourIntent = new Intent(Submit.this, ourClass);
				startActivity(ourIntent);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
      }
   }
    // Given a URL, establishes an HttpUrlConnection and retrieves
    // the web page content as a InputStream, which it returns as
    // a string.
    private String downloadUrl(String myurl) throws IOException {
    	InputStream is = null;
    	// Only display the first 500 characters of the retrieved
    	// web page content.
    	int len = 2500;
         
    	try {
    		URL url = new URL(myurl);
    		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    		conn.setReadTimeout(10000 /* milliseconds */);
    		conn.setConnectTimeout(15000 /* milliseconds */);
    		conn.setRequestMethod("GET");
    		conn.setDoInput(true);
    		// Starts the query
    		conn.connect();
    		int response = conn.getResponseCode();
    		
    		// Check to see if our order submission failed HERE
    		// 
    		if(response != 200)
    			System.out.println("Order Submission Failed");  
    		else
    			System.out.println("Order Submission Successful");
    		////////////////////////////////////////////////////////////

  //  		Log.d(DEBUG_TAG, "The response is: " + response);
    		is = conn.getInputStream();

    		// Convert the InputStream into a string
    		String contentAsString = readIt(is, len);
    		return contentAsString;
         
    		// Makes sure that the InputStream is closed after the app is
    		// finished using it.
    	} finally {
    		if (is != null) {
    			is.close();
    		} 
    	}
    	
    }
    
    // Reads an InputStream and converts it to a String.
    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");        
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
