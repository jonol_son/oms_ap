package com.comp.oms;

class Node <T> {
	T item;
	Node <T> next;
	String word; 
	
	public Node (T newItem){
		item = newItem;
		next = null;
	}
	
	public Node (T newItem, Node<T> nextNode) {
		item = newItem;
		next = nextNode;
	}
	public String toString() {
		return item.toString();
	}
}
