package com.comp.oms;

public class ListReferenceBased <T extends Comparable<? super T>> implements ListInterface<T> {
	// reference to linked list of items
	private boolean isEmpty, isSorted;
	private Node<T> head;
	private String text;
	private int count;
	private int diff;
	
	public ListReferenceBased(){
		head = null;
	} //end default constructor
	
	public boolean isEmpty() {
		if (head == null)
			isEmpty = true;
		else
			isEmpty = false;
		return isEmpty;
	} // end isEmpty
	
	public boolean isSorted() {
		//checks if list is empty, if it is it is sorted
		isSorted = isEmpty();
		if (isSorted == true)
		{
			return isSorted;
		}
		else
		{
			//Goes through the list and checks if each set of items are in sorted order.  
			//If any pair are out of order sets sorted to false
			Node<T> curr = head;
			isSorted = true;
			
			while (isSorted == true && curr.next != null){
				if (curr.toString().compareTo(curr.next.toString()) <= 0)
					isSorted = true;
				else
					isSorted = false;
				curr = curr.next;
			}
			
		}
		return isSorted;
	} // end isSorted
	
	public int size() {
		Node<T> curr = head;
		count = 0;
		while (curr != null)
		{
			++count;
			curr = curr.next;
		}
		return count;
	} //end size
	
/*
Locates a node in a linked list
Precondition: index is the number of the desired node.  
Postcondition: Returns a reference to the desired node.
 */
	private Node<T> find (int index) {
		Node<T> curr = head;
		for (int skip = 0; skip < index; skip++){
			curr = curr.next;
		}
		return curr;
	} // end find
	
	/*
	Gets the object of node in a linked list
	Precondition: index is the number of the desired node.  
	Postcondition: Returns the object assopciate with a desired node.
	 */
	public T get(int index){
		if (index >= 0 && index < size()) {
			Node<T> curr = find(index);
			T dataItem = curr.item;
			return dataItem;
		}
		return null;
	}// end Get
	
	public void add (int index, T item) {
		index = index-1;
		if (index >= 0 && index < (size()+1)){
			if (index == 0){
				Node<T> newNode = new Node<T> (item, head);
				head = newNode;
			}
			else {
				Node<T> prev = find (index-1);
				Node<T> newNode = new Node<T> (item, prev.next);
				prev.next = newNode;
			}
		}
		else { 
		System.out.println("Invalid Position.");
		System.out.println(" ");
		}
	}
	public void add (T item) {
		if (isEmpty())
		{
			Node<T> newNode = new Node<T> (item, head);
			head = newNode;
		}
		else {
			Node<T> prev = find (size());
			Node<T> newNode = new Node<T> (item, prev.next);
			prev.next = newNode;
		}
	}
	/*
	Removes a node from a linked list
	Precondition: index is the number of the desired node.  
	Postcondition: unlinks a reference to the desired node.
	 */
	/*public void remove(T item){
		Node<T>curr = head;
		if (curr.toString().compareTo(item.toString()) <= 0) {
			head = head.next;
		}
		else {
			
		}
	}*/
	
	public void remove(int index) {
		index = index-1;
		if (index>= 0 && index < size()) {
			if(index==0)
				head = head.next;
			else {
				Node<T> prev = find(index-1);
				Node<T> curr = prev.next;
				prev.next = curr.next;
			}
		}
		else {
			System.out.println("One cannot remove an item that does not exit");
			System.out.println(" ");
		}	
	}
	/*
	Reverses the order of a linked list
	Precondition: requires a Listreferencebased object  
	Postcondition: Reverses the linking order of a linked list
	 */
	public void reverseList() {
		if (isEmpty() != true){
			Node<T> curr = head;
			Node<T> next = head.next;
			head.next = null;
			
			while (next !=null) {
				Node<T> loop = next.next;
				next.next = curr;
				curr = next;
				next = loop;
			}
			head = curr;
		}
		
		
	}
	
	//Formats the String to list items in list, display number of items and display if it is sorted
	public String toString() {
		Node<T> curr = head;
		if (curr == null) {
			text = "The List is empty, you should add something";
		}
		else {
			text = "List: ";
			while(curr != null) {
				text = text + curr.toString() + " ";
				curr = curr.next;
			}
			text = text + "\nNumber of items: " + size() + "\n";
			if (isSorted() == true)
				text = text + "List is in sorted order \n";
			else
				text = text + "List is not in sorted order \n";
		}
		return text;
	}

	@Override
	public void removeAll() {
		// TODO Auto-generated method stub
		head = null;
	}



		
}


