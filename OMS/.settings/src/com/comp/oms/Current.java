package com.comp.oms;

import java.util.ArrayList;
import java.util.List;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class Current extends Activity implements View.OnClickListener {

	Button add, remove, submit;
	//array for added items
	List<String> SpinnerArray = new ArrayList<String>();
	//array for table number
	List<Integer> SpinnerArray2 = new ArrayList<Integer>();
	ListReferenceBased<String> Order = new ListReferenceBased<String>();
	String list[] = { "Add" };
	String order, newItem;
	Spinner addedItems;
	//FOR DEBUGGING
	TextView display;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub

		super.onCreate(savedInstanceState);
		setContentView(R.layout.current);
		add = (Button) findViewById(R.id.addItem);
		remove = (Button) findViewById(R.id.removeItem);
		submit = (Button) findViewById(R.id.submitOrder);
		//FORDEBUG
		display = (TextView) findViewById(R.id.debugg);
		//initially draw order spinner
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, SpinnerArray);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		addedItems = (Spinner) findViewById(R.id.spinner1);
		addedItems.setAdapter(adapter);
		add.setOnClickListener(this);
		submit.setOnClickListener(this);
		remove.setOnClickListener(this);
		//for tables
		fillTableSpinner();
		

		/*
		 * //TEST XXXXXXXXXXXXXXX Intent ourIntent = getIntent(); String next =
		 * (String)ourIntent.getSerializableExtra("order"); int count = 0; if
		 * (Order.isEmpty() == true){ list[0] = next; Order.add(0, next); } else
		 * { Order.add(Order.size(), next); while (Order.isEmpty()!=true){
		 * list[count] = Order.get(count); count++; } }
		 */
		// set up list adapter using a stock int as 2nd input
		// setListAdapter(new ArrayAdapter<String>(Current.this,
		// android.R.layout.simple_list_item_1, list));
	}

	private void fillTableSpinner() {
		// TODO Auto-generated method stub
		for (int i=1; i<20; i++){
		SpinnerArray2.add(i);
		
		}
		ArrayAdapter<Integer> adapter2 = new ArrayAdapter<Integer>(this,
				android.R.layout.simple_spinner_item, SpinnerArray2);
		adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner tableNumber = (Spinner) findViewById(R.id.spinner2);
		tableNumber.setAdapter(adapter2);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.addItem:

			// set up intent with class variable, need catch incase
			// classnotfound
			// will start a class variable with WHICHEVER string is selected
			// In mainfest you nolonger need intent filter if you use this

			Intent i = new Intent(this, Menu.class);
			startActivityForResult(i, 1);
			break;

		case R.id.removeItem:
		/*	String curr = addedItems.getSelectedItem().toString();
			Order.remove(curr);
			int count = 0;
			if (Order.isEmpty()) {
				SpinnerArray.add(list[0]);
			} else {
				while (Order.size() != count) {
					list[count] = Order.get(count);
					SpinnerArray.add(list[count]);
					count++;
				}
			}
			//REDRAW SPINNER after returning
			ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
					android.R.layout.simple_spinner_item, SpinnerArray);
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			addedItems.setAdapter(adapter);
			break;
*/
		case R.id.submitOrder:
			// XMIT!
			break;

		}
	}

	// @Override
	// FILLING SPINNER WITH LIST
	public void setDebug(String next) {
		display.setText("WTF?!?");
	}
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		if (requestCode == 1) {

			if (resultCode == RESULT_OK) {
				Toast.makeText(this, data.getData().toString(), Toast.LENGTH_SHORT).show();
				String choice=data.getStringExtra("choice"); 
				Order.add(choice);
				int count = 0;
				if (Order.isEmpty()) {
					list[0] = "Add";
					SpinnerArray.add(list[0]);
				} else {
					while (Order.size() != count) {
						list[count] = Order.get(count);
						SpinnerArray.add(list[count]);
						count++;
					}
				}
				//REDRAW SPINNER after returning
				ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
						android.R.layout.simple_spinner_item, SpinnerArray);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				addedItems.setAdapter(adapter);
			}

			if (resultCode == RESULT_CANCELED) {
				// Write your code if there's no result
			}
		}
	}// onActivityResult

}
