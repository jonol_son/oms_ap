package com.comp.oms;


import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Item extends ListActivity {

	String classes[] = { "Linguine", "spagetti", "Lasagna", "angelhair" } ;

	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//set up list adapter using a stock int as 2nd input
		setListAdapter(new ArrayAdapter<String>(Item.this, android.R.layout.simple_list_item_1, classes));
	}
	
	
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		//Cheese gets position clicked
		String listItem = classes[position];
		//set up intent with class variable, need catch incase classnotfound
		//will start a class variable with WHICHEVER string is selected
		//In mainfest you nolonger need intent filter if you use this
		try {
		Class ourClass = Class.forName("com.comp.oms." + listItem);
		Intent ourIntent = new Intent(Item.this, ourClass);
		startActivity(ourIntent);
		}catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	

}
