package com.comp.oms;

public interface ListInterface<T> {
// list of operations:
	public boolean isEmpty();
	public boolean isSorted();
	public int size();
	public void add(int index, T item);
	public void remove(int index);
	public T get(int index);
	public void removeAll();
}  //end ListInterface
