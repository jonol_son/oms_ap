package com.comp.oms;

import android.app.ListActivity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Menu extends ListActivity {
	ListReferenceBased<String> subMenu = new ListReferenceBased();
	String mainMenu[] = { "Appetzers", "Soup", "Sandwhich", "Pizza", "Pasta",
			"Dessert", "Beverages" };
	int count = 0, catergory;
	String choice;
	String appetizer[] = { "Nachos", "Hummus", "Crab Cakes",
			"Spinache and Artichoke", "Buffalo Wings", "Stuffed Cabbage Rolls",
			"Stuffed Mashrooms" };
	String soup[] = { "WeddingSoup", "Minestrone", "ClamChowder",
			"CornChowder", "Chili" };
	String sandwhich[] = { "Beef", "Chicken", "Portabella", "Crab",
			"MysteryMeat", "Baolgnia", "EggSalad", "chickenSalad" };
	String pizza[] = { "Cheese", "Peperoni", "Sausage", "Combonation" };
	String pasta[] = { "Linguine", "Spagetti", "Lasagna", "Angelhair" };
	String dessert[] = { "IceCream", "AppleFritter", "BlueberryCheeseBake",
			"Brownie", "Popsicle" };
	String beverages[] = { "Coke", "DietCoke", "Sprite", "Rootbeer",
			"AppleJuice", "Orange Juice", "BottleedWater" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		// set up list adapter using a stock int as 2nd input
		setListAdapter(new ArrayAdapter<String>(Menu.this,
				android.R.layout.simple_list_item_1, mainMenu));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		// Cheese gets position clicked
		// String subMenuHeading = mainMenu[position];
		// set up intent with class variable, need catch incase
		// classnotfound
		// will start a class variable with WHICHEVER string is selected
		// In mainfest you nolonger need intent filter if you use this
		if (count == 0){
			catergorySwitch(position);
			count++;
		}
		else if (count ==1){
			choice = itemSwitch(position);
			Intent returnIntent = new Intent();
			returnIntent.putExtra("choice", choice);
			returnIntent.setData(Uri.parse(choice));
			setResult(RESULT_OK, returnIntent);
			finish();
		}

		/*
		 * Intent data = new Intent(); data.setData(Uri.parse(choice));
		 * setResult(RESULT_OK,data); finish();
		 */

	}

	private void catergorySwitch(int position) {
		// If initial menu is selected chooses which submenu to display
		switch (position) {
		// Appetizer
		case 0:
			catergory = 0;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, appetizer));
			break;
		// Soup
		case 1:
			catergory = 1;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, soup));
			break;
		// Salad
		case 2:
			catergory = 2;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, sandwhich));
			break;
		// Pizza
		case 3:
			catergory = 3;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, pizza));
			break;
		// Pasta
		case 4:
			catergory = 4;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, pasta));
			break;
		// Dessert
		case 5:
			catergory = 5;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, dessert));
			break;
		// Beverages
		case 6:
			catergory = 6;
			setListAdapter(new ArrayAdapter<String>(Menu.this,
					android.R.layout.simple_list_item_1, beverages));
			break;
		}
	}

	private String itemSwitch(int position) {
		// TODO Auto-generated method stub
		// If initial menu is selected chooses which submenu to display

		if (catergory == 0) {
			choice = appetizer[position];
		} else if (catergory == 1) {
			choice = soup[position];
		} else if (catergory == 2) {
			choice = sandwhich[position];
		} else if (catergory == 3) {
			choice = pizza[position];
		} else if (catergory == 4) {
			choice = pasta[position];
		} else if (catergory == 5) {
			choice = dessert[position];
		} else if (catergory == 6) {
			choice = beverages[position];
		}
		return choice;

	}

}
