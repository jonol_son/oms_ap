package com.comp.oms;
 
import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
	
		int choice = 1, position = 0;
		String item;
		Scanner input = new Scanner (System.in);
		//Constructs empty list
		ListReferenceBased<String> main = new ListReferenceBased();
		
		do{
			//Displays menu
			System.out.println("1) Add item");
			System.out.println("2) Remove item");
			System.out.println("3) Reverse list");
			System.out.println("4) Exit program");
			System.out.println(" ");
			System.out.print("Enter your Choice (1-4): ");
			choice = input.nextInt();
			
			switch (choice){
			case 1:  //Adds input into list
				System.out.print("Enter position: ");
				position = input.nextInt();
				//Checks to see if selection is in bound
				if ((position <= 0)|| position > main.size()+1) {
					System.out.println("Invalid position");
					System.out.println(" ");
				}
				else {
					//Takes input for item
					System.out.print("Enter item to add: ");
					item = input.next();
					main.add(position, item);
					System.out.println(main);
				}
				break;
				
			case 2:  //Removes item from list based on position
				System.out.print("Enter position: ");
				position = input.nextInt();
				if (position < 0) {
					System.out.println("Invalid position");
				}
				else {
					main.remove(position);
					System.out.println(main);
				}
				break;
				
			case 3:  //Reverse
				main.reverseList();
				System.out.println(main);
				break;
				
			case 4:  //Exit
				System.out.println(" ");
				System.out.println("Goodbye");
				break;
			}
		}while (choice != 4);
	}

}
